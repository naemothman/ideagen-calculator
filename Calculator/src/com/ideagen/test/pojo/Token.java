package com.ideagen.test.pojo;


/**
 * @author mohd.naem
 * @since 1.0
 */
public class Token {

	public static final int UNKNOWN = -1;

	public static final int NUMBER = 0;

	public static final int OPERATOR = 1;

	public static final int LEFT_PARENTHESIS = 2;

	public static final int RIGHT_PARENTHESIS = 3;

	private int type;

	private double value;

	private char operator;

	private int precedence;


	/**
	 * Constructor to set type and value
	 * 
	 * @param x
	 * @since 1.0
	 */
	public Token(double x) {
		type = NUMBER;
		value = x;
	}


	/**
	 * Constructor to set type, value, operator, precedence based on contents
	 * 
	 * @param contents
	 *             token from arithmetic expression
	 * @since 1.0
	 */

	public Token(String contents) {
		switch (contents) {
		case "+":
			type = OPERATOR;
			operator = contents.charAt(0);
			precedence = 1;
			break;
		case "-":
			type = OPERATOR;
			operator = contents.charAt(0);
			precedence = 1;
			break;
		case "*":
			type = OPERATOR;
			operator = contents.charAt(0);
			precedence = 2;
			break;
		case "/":
			type = OPERATOR;
			operator = contents.charAt(0);
			precedence = 2;
			break;
		case "(":
			type = LEFT_PARENTHESIS;
			break;
		case ")":
			type = RIGHT_PARENTHESIS;
			break;
		default:
			type = NUMBER;
			try {
				value = Double.parseDouble(contents);
			} catch (Exception ex) {
				type = UNKNOWN;
			}
		}
	}


	/**
	 * Perform arithmetic operation based on operator
	 * 
	 * @param a
	 *             first numeric value
	 * @param b
	 *             second numeric value
	 * @return result
	 * @see Token
	 * @since .0
	 */
	public Token operate(double a, double b) {
		double result = 0;
		switch (operator) {
		case '+':
			result = a + b;
			break;
		case '-':
			result = a - b;
			break;
		case '*':
			result = a * b;
			break;
		case '/':
			result = a / b;
			break;
		}
		return new Token(result);
	}


	/**
	 * @return type
	 * @since 1.0
	 */
	public int getType() {
		return type;
	}


	/**
	 * @return value
	 * @since 1.0
	 */
	public double getValue() {
		return value;
	}


	/**
	 * @return precedence
	 * @since 1.0
	 */
	public int getPrecedence() {
		return precedence;
	}

}
