package com.ideagen.test.pojo;


import java.util.ArrayList;


/**
 * @author mohd.naem
 * @since 1.0
 */
public class TokenStack {

	private ArrayList<Token> tokens;


	/**
	 * Constructor to initialize tokens
	 * 
	 * @since 1.0
	 */
	public TokenStack() {
		tokens = new ArrayList<>();
	}


	/**
	 * Check if list has elements
	 * 
	 * @return true if tokens is empty, false if token is not empty
	 * @since 1.0
	 */
	public boolean isEmpty() {
		return tokens.size() == 0;
	}


	/**
	 * Get last element of from list
	 * 
	 * @return last element of list
	 * @since 1.0
	 */
	public Token last() {
		return tokens.get(tokens.size() - 1);
	}


	/**
	 * Add token to list
	 * 
	 * @param token
	 *             token to be add to list
	 * @see Token
	 * @since 1.0
	 */
	public void add(Token token) {
		tokens.add(token);
	}


	/**
	 * Remove the last element from list
	 * 
	 * @since 1.0
	 */
	public void remove() {
		tokens.remove(tokens.size() - 1);
	}

}