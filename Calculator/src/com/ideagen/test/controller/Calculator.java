package com.ideagen.test.controller;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import com.ideagen.test.pojo.Token;
import com.ideagen.test.pojo.TokenStack;
import com.ideagen.test.util.BaseUtil;


/**
 * Calculate String arithmetic expression
 *
 * @author mohd.naem
 * @since 1.0
 */
public class Calculator {

	private static TokenStack operatorList;

	private static TokenStack numericList;

	private static boolean isError;


	/**
	 * Main method to run calculation of string arithmetic expressionW
	 *
	 * @param args
	 *             arguments input
	 * @since 1.0
	 */

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String userInput, finalResult = null;
		double result = 0.00;
		StringBuffer sb = null;
		String errorDesc = null;

		// User input optional (1. Custom expression, 2. Predefined
		// expression)
		System.out.println(
				"Choose option to proceed: \n 1. Custom expression (separated by spaces) \n 2. Predefined expression");
		userInput = input.nextLine();

		if (BaseUtil.isEquals(userInput, "1")) {
			sb = new StringBuffer();
			System.out.println("Enter arithmetic expression (seperated by spaces): ");
			userInput = input.nextLine();
			result = Calculator.calculate(userInput);
			sb.append(userInput).append(" = ").append(result);
		} else if (BaseUtil.isEquals(userInput, "2")) {
			sb = new StringBuffer();
			for (String expression : getSampleExpression()) {
				result = Calculator.calculate(expression);
				sb.append(expression).append(" = ").append(result).append("\n");
			}
		} else {
			errorDesc = "Wrong option entered. ";
		}

		// determine output to be shown to user, either error or arithmetic
		// operation result

		if (isError) {
			errorDesc = "Error : Invalid arithmetic expression.";
		}

		if (BaseUtil.isObjNull(errorDesc)) {
			finalResult = sb.toString();
		} else {
			finalResult = errorDesc;
		}
		System.out.println("\n---------- Result ----------\n");
		System.out.println(finalResult);
		input.close();
	}


	/**
	 * Calculate string arithmetic expression
	 *
	 * @param sum
	 *             string arithmetic expression
	 * @return result of arithmetic operation
	 * @since 1.0
	 */
	public static double calculate(String sum) {
		double resultCalc = 0.00;
		operatorList = new TokenStack();
		numericList = new TokenStack();

		// split the string arithmetic expression by spaces
		String[] sumSplit = sum.split(" ");
		List<String> partsList = Arrays.asList(sumSplit);
		List<Token> tokenList = null;
		if (!BaseUtil.isListNull(partsList)) {
			tokenList = new ArrayList<>();
			for (String part : partsList) {
				tokenList.add(new Token(part));
			}

			// split to 2 different list for numeric(numericList) and non
			// numeric(operatorList) and perform arithmetic operation
			for (Token token : tokenList) {
				int tokenType = token.getType();
				if (tokenType == Token.NUMBER) {
					numericList.add(token);
				} else if (tokenType == Token.OPERATOR) {
					if (operatorList.isEmpty() || token.getPrecedence() > operatorList.last().getPrecedence()) {
						operatorList.add(token);
					} else {
						while (!operatorList.isEmpty()
								&& token.getPrecedence() <= operatorList.last().getPrecedence()) {
							processOperator(operatorList.last());
						}
						operatorList.add(token);
					}
				} else if (token.getType() == Token.LEFT_PARENTHESIS) {
					operatorList.add(token);
				} else if (token.getType() == Token.RIGHT_PARENTHESIS) {
					while (!operatorList.isEmpty() && operatorList.last().getType() == Token.OPERATOR) {
						processOperator(operatorList.last());
					}
					if (!operatorList.isEmpty() && operatorList.last().getType() == Token.LEFT_PARENTHESIS) {
						operatorList.remove();
					} else {
						isError = true;
					}
				}
			}

			// perform arithmetic operation on remaining elements in
			// operatorList
			while (!operatorList.isEmpty() && operatorList.last().getType() == Token.OPERATOR) {
				processOperator(operatorList.last());
			}

			// get final result of arithmetic operation and empty the
			// numericList
			if (!isError) {
				Token result = null;
				if (!numericList.isEmpty()) {
					result = numericList.last();
					numericList.remove();
				} else {
					isError = true;
				}

				if (!operatorList.isEmpty() || !numericList.isEmpty()) {
					isError = true;
				} else {
					resultCalc = result.getValue();
				}
			}
		}
		return resultCalc;
	}


	/**
	 * Process operator based on arithmetic operation of t
	 *
	 * @param t
	 *             token to perform arithmetic operation
	 * @see Token
	 * @since 1.0
	 */
	private static void processOperator(Token t) {
		operatorList.remove();

		Token first = null, second = null;
		if (numericList.isEmpty()) {
			isError = true;
			return;
		} else {
			second = numericList.last();
			numericList.remove();
		}

		if (numericList.isEmpty()) {
			isError = true;
			return;
		} else {
			first = numericList.last();
			numericList.remove();
		}
		Token result = t.operate(first.getValue(), second.getValue());
		numericList.add(result);
	}


	/**
	 * Sample string arithmetic expression for testing
	 *
	 * @return predefined sample expression
	 * @since 1.0
	 */
	private static List<String> getSampleExpression() {
		List<String> expressionList = new ArrayList<>();
		expressionList.add("1 + 1");
		expressionList.add("2 * 2");
		expressionList.add("1 + 2 + 3");
		expressionList.add("6 / 2");
		expressionList.add("11 + 23");
		expressionList.add("11.1 + 23");
		expressionList.add("1 + 1 * 3");
		expressionList.add("( 11.5 + 15.4 ) + 10.1");
		expressionList.add("23 - ( 29.3 - 12.5 )");
		expressionList.add("10 - ( 2 + 3 * ( 7 - 5 ) )");
		return expressionList;
	}

}