package com.ideagen.test.util;


import java.util.List;


/**
 * @author mohd.naem
 * @since 1.0
 */
public class BaseUtil {

	public static final String EMPTY_STRING = "";


	/**
	 * Check if object is null or empty
	 * 
	 * @param obj
	 *             object to be check for null or empty
	 * @return true if obj null or empty, false if obj not null and not empty
	 * @see Object
	 * @since 1.0
	 */
	public static boolean isObjNull(Object obj) {
		if (obj != null && getStr(obj).length() > 0) {
			return false;
		} else {
			return true;
		}
	}


	/**
	 * Check if list is null or empty
	 * 
	 * @param o
	 *             list to be check for null or empty
	 * @return true if o is null or empty, false if o is not null and not empty
	 * @see List
	 * @since 1.0
	 */
	public static boolean isListNull(List o) {
		if (o != null && !o.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}


	/**
	 * Get string value of object
	 * 
	 * @param obj
	 *             obj to be get the string value
	 * @return trim string if obj not null, empty string if obj is null
	 * @see Object
	 * @since 1.0
	 */
	public static String getStr(Object obj) {
		if (obj != null) {
			return obj.toString().trim();
		} else {
			return EMPTY_STRING;
		}
	}


	/**
	 * @param oriSrc
	 *             original string to be compare
	 * @param compareSrc
	 *             compared string to be compare with oriSrc
	 * @return true if oriSrc and compareSrc is equal, false if oriSrc and
	 *         compareSrc not equal
	 * @since 1.0
	 */
	public static boolean isEquals(String oriSrc, String compareSrc) {
		if (oriSrc != null && getStr(oriSrc).equals(getStr(compareSrc))) {
			return true;
		} else {
			return false;
		}
	}
}
